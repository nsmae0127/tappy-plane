﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public enum GameState { NONE, MAIN_MENU, TUTORIAL, WAIT_GAME, START, IN_GAME, RANKING, PAUSE, GAME_OVER }

public class GameManager : MonoBehaviour {

    public GameState gameState { get; private set; }

    static GameManager _instance;
    public GameObject player;

    Vector3 initialPositionPlayer;

    public GameObject SCORE_UI;

    int score;

    public static GameManager instance
    {
        get
        {
            if (_instance == null )
            {
                _instance = FindObjectOfType( typeof( GameManager ) ) as GameManager;

                if (_instance == null )
                {
                    var go = new GameObject( "_gameManager" );
                    DontDestroyOnLoad( go );
                    _instance = go.AddComponent<GameManager>();
                    _instance.player = GameObject.FindGameObjectWithTag( "Player" );
                }
            }
            return _instance;
        }
    }

	void Start () {
        gameState = GameState.TUTORIAL;
        initialPositionPlayer = player.GetComponent<Transform>().position;
	}
	
	void Update () {
        switch ( gameState )
        {
            case GameState.START:
                StartGame();
                break;
            case GameState.TUTORIAL:
                GameTutorial();
                break;
        }
	}

    public void StartGame()
    {
        ResetPositionPlayer();
        SetKinematicRigidBody(false);
        gameState = GameState.IN_GAME;
        GameReady();
        //ExtensionMethods.FindAndSetActive( "_obstacles", typeof( GameObject ), true );
    }

    public void GameTutorial()
    {
        ResetPositionPlayer();
        SetKinematicRigidBody( true );
    }

    private void ResetPositionPlayer()
    {
        player.transform.eulerAngles = Vector3.zero;
        player.GetComponent<Transform>().position = initialPositionPlayer;
        player.SetActive( true );
    }

    private void SetKinematicRigidBody(bool isKinematic)
    {
        player.GetComponent<Rigidbody2D>().isKinematic = isKinematic;
        return;
    }

    public void AddPoints()
    {
        score++;

        SetScoreValue( score );
    }

    public void SetScoreValue(int value)
    {
        score = value;

        var scoreText = SCORE_UI.FindChildObject<Text>("Score");
        scoreText.text = value.ToString();

        var scoreShadowText = SCORE_UI.FindChildObject<Text>( "ScoreShadow" );
        scoreShadowText.text = value.ToString();
    }

    public void GameReady()
    {
        SCORE_UI.SetActive( true );
    }
}
