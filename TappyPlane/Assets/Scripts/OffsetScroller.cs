﻿using UnityEngine;
using System.Collections;

public class OffsetScroller : MonoBehaviour {

    public float scrollSpeed;
    private Renderer renderer;
    private Vector2 savedOffset;

    void Awake()
    {
        renderer = GetComponent<Renderer>();
    }

	void Start () {
        savedOffset = renderer.sharedMaterial.GetTextureOffset( "_MainTex" );
	}
	
	void Update () {

        float x = Mathf.Repeat(Time.time * scrollSpeed, 1);

        Vector2 offset = new Vector2( x, savedOffset.y );

        renderer.sharedMaterial.SetTextureOffset( "_MainTex", offset );

	}

    void OnDisable()
    {
        renderer.sharedMaterial.SetTextureOffset( "_MainTex", savedOffset );
    }
}
