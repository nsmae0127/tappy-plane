﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

    private Rigidbody2D playerRigidBody2D;
    private Animator playerAnimator;

    public float gravityForce;
    public float speedAngle = 0.1f;
    public float speedForce = 0.1f;
    public float engineForceOnFalling;

    private GameManager GM;

	void Start () {
        playerRigidBody2D = GetComponent<Rigidbody2D>();
        playerAnimator = GetComponent<Animator>();
        GM = GameManager.instance;
    }
	
	void Update () {
        if ( Input.GetMouseButtonDown( 0 ) && GM.gameState == GameState.IN_GAME && GM.gameState != GameState.GAME_OVER)
        {
            playerRigidBody2D.velocity = Vector2.zero;
            playerRigidBody2D.AddForce(new Vector2(0, speedForce) * gravityForce);
        } else if (Input.GetMouseButtonDown(0) && GM.gameState == GameState.TUTORIAL) {
            GM.StartGame();
        }

        ChangeAngle();
	}

    void ChangeAngle()
    {
        if (playerRigidBody2D.velocity.y < 0)
        {
            var newAngle = Quaternion.Euler( new Vector3(0, 0, -30) );
            var currentAngle = Quaternion.Euler( transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z );
            transform.rotation = Quaternion.Slerp(currentAngle, newAngle, speedAngle);
            playerAnimator.speed = engineForceOnFalling;
        }
        else if ( playerRigidBody2D.velocity.y > 0 )
        {
            var newAngle = Quaternion.Euler( new Vector3( 0, 0, 30 ) );
            var currentAngle = Quaternion.Euler( transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z );
            transform.rotation = Quaternion.Slerp( currentAngle, newAngle, speedAngle );
            playerAnimator.speed = 1;
        }
    }

    void OnCollisionEnter2D(Collision2D collision2D)
    {
        gameObject.SetActive( false );
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Point")
        {
            GM.AddPoints();
        }
    }
}
