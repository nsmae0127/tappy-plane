﻿using UnityEngine;
using System.Collections;

public class RockController : MonoBehaviour {

    public float speedMovement;

    private GameManager GM;

    void Start ()
    {
        GM = GameManager.instance;
    }

	void Update () {

        if (GM.gameState != GameState.IN_GAME)
        {
            return;
        }

        transform.position += new Vector3( speedMovement, 0, 0 ) * Time.deltaTime;

        if (transform.position.x < -13)
        {
            gameObject.SetActive( false );
        }
	}
}
