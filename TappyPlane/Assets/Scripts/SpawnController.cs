﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnController : MonoBehaviour {

    private float currentRateSpawn;

    public float minHeight;
    public float maxHeight;
    public float rateSpawn;
    public int maxSpawnRocks;
    public GameObject rockPrefab;
    public List<GameObject> rockList;

    private GameManager GM;

    void Start () {
        currentRateSpawn = rateSpawn;
        rockList.InstantiateObjects(5, rockPrefab);
        GM = GameManager.instance;
	}
	
	void Update () {

        if ( GM.gameState != GameState.IN_GAME )
        {
            return;
        }

        currentRateSpawn += Time.deltaTime;

        if (currentRateSpawn > rateSpawn)
        {
            currentRateSpawn = 0;
            Spawn();
        }
	}

    void Spawn()
    {
        float randHeight = Random.Range( minHeight, maxHeight );

        GameObject tempRock = rockList.ReturnActive();

        if (tempRock != null)
        {
            tempRock.transform.position = new Vector3(transform.position.x, randHeight, transform.position.z);
            tempRock.SetActive( true );
        }
    }
}
